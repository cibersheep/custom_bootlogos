Custom Bootlogos
==========================
- Black Logos now avaible

- Modified Bootlogos For Ubuntu Touch devices

- Disclaimer
  ==========

  - Please note, I am not responsible for any device break/brick, this bootlogos are modifications use at your own risk.
    ====================================================================================================================

- If you like my work please feel free to donate me a coffe:
  =====================================================
  - https://www.paypal.me/rubencarneiro

Nexus 5
=======
**How to flash**:

- Reboot device to fastboot
- `fastboot flash imgdata imgdata.img`


Fairphone 2
===========
**How to flash**:

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash splash splash.img`


Meizu MX4
=========
**How to flash**:

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ E4.5
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ E5
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ M10HD
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ M10FHD
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


OnePlus One
=========
**How to flash**

- Reboot device to fastboot
- In a terminal do:
- `fastboot flash LOGO logo.bin`

OePlus 3/3T
=========
**How to flash**

- Reboot device to fastboot
- In a terminal do:
- `fastboot flash LOGO logo.bin`
- for some reason some people are unable to flash trough fastboot "FAILED (remote: Partition flashing is not allowed)"
- Use the update-zip and flash trough recovery: https://drive.google.com/file/d/1MBA7yJREcnQ_Pg8D3_cY_VNry-L4_aSc/view

Volla Phone
=========
**How to flash**

- Reboot device to fastboot
- In a terminal do:
- `fastboot flash logo logo.bin`